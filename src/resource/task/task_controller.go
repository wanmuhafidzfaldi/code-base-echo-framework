package task

import (
	"github.com/labstack/echo"
	"net/http"
)

func Create(c echo.Context ) error {
	u := new(Todo)
	if err := c.Bind(u); err != nil {
		return err
	}
	respone, data := insert(Todo{u.Type, u.Remember})

	return c.JSON(http.StatusOK, struct {
		Response
		Data Todo `json:"data"`
	}{respone, data})
}

func Show(c echo.Context ) error {

	respone, data := show()

	return c.JSON(http.StatusOK, struct {
		Response
		Data []Todo `json:"data"`
	}{respone, data})
}

func Update(c echo.Context) error {
	u := new(Todo)
	if err := c.Bind(u); err != nil {
		return err
	}

	respone, data := show()

	return c.JSON(http.StatusOK, struct {
		Response
		Data []Todo `json:"data"`
	}{respone, data})
}