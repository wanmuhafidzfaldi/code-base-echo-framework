package task

type Todo struct {
	Type 		string `json:"type"`
	Remember	string `json:"remember"`
}

type Response struct {
	Code 	int		`json:"code"`
	Status	bool	`json:"status"`
	Message	string	`json:"message"`
}

func response(code int, status bool, message string) Response {
	return Response{code, status, message}
}