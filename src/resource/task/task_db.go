package task

import (
	"code-base-echo-framework/src/database/mongodb"
	"fmt"
)

func insert(todo Todo) (Response, Todo) {
	var session, err = mongodb.ConnectMongodb()

	if (err != nil) {
		fmt.Println("Error!", err.Error())
		return response(500, false, "Error Message"), todo
	}
	defer session.Close()

	var collection = session.DB("belajar_mongodb_golang").C("task")
	err = collection.Insert(todo)
	if (err != nil) {
		fmt.Println("Error!", err.Error())
		return response(500, false, "Error Message"), todo
	}

	return response(200, true, "Insert Success"), todo
}

func update(todo Todo) (Response, Todo) {
	return response(200, true, "Insert Success"), todo
}

func show() (Response, []Todo) {
	var todo []Todo

	var session, err = mongodb.ConnectMongodb()

	if (err != nil) {
		fmt.Println("Error!", err.Error())
		return response(500, false, "Error Message"), todo
	}
	defer session.Close()

	var collection = session.DB("belajar_mongodb_golang").C("task")
	err = collection.Find(nil).All(&todo)
	if (err != nil) {
		fmt.Println("Error!", err.Error())
		return response(500, false, "Error Message"), todo
	}

	return response(200, true, "Success Get List Data"), todo
}