package routes

import (
	"github.com/labstack/echo"
	"code-base-echo-framework/src/resource/task"
)

func NewRouter() *echo.Echo  {
	e := echo.New()

	var respone = struct {
		Status  bool    `json:"status"`
		Message string  `json:"message"`
	}{}
	respone.Status = true
	respone.Message = "This Server Running"

	e.GET("/", func(context echo.Context) error {
		return  context.JSON(200, respone )
	})

	e.GET("/task", task.Show)
	e.POST("/task", task.Create)
	e.PUT("/task/:id", task.Update)

	return e
}