package main

import "code-base-echo-framework/src/routes"

func main() {
	app := routes.NewRouter();
	app.Logger.Fatal(app.Start(":8080"))
}